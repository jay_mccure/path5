#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0

import copy
import argparse
from functools import wraps
import json
import logging
from pathlib import Path
from socket import gethostname
from typing import Any, Dict, List, Literal, Optional
import yaml
import ldap3
import requests
import sys

logger = logging.getLogger(__name__)


Config = Dict[str, Any]
USER_AGENT = (
    "WMCS Maintain-Harbor/1.0 tool-maintain-harbor@"
    f"{gethostname()} {requests.utils.default_user_agent()}"
)
TOOLFORGE_PROJECT_PREFIX = "tool-"


IMAGE_RETENTION_POLICY_TEMPLATE = {
    "algorithm": "or",
    "rules": [
        {
            "action": "retain",
            "params": {"latestPushedK": 5},
            "scope_selectors": {
                "repository": [
                    {
                        "decoration": "repoMatches",
                        "kind": "doublestar",
                        "pattern": "**",
                    }
                ]
            },
            "tag_selectors": [
                {
                    "decoration": "matches",
                    "extras": '{"untagged":false}',
                    "kind": "doublestar",
                    "pattern": "**",
                }
            ],
            "template": "latestPushedK",
        }
    ],
    "scope": {
        "level": "project",
        # update this with the project id
        "ref": None,
    },
    "trigger": {"kind": "Schedule", "settings": {"cron": "0 */5 * * * *"}},
}


class APIError(Exception):
    """Simple custom error class for harbor endpoints api errors"""

    def __init__(self, description: str, reason: str):
        self.description = description
        self.reason = reason

    def to_str(self):
        try:
            reason = ""
            for err in json.loads(self.reason)["errors"]:
                reason += f"\n{err['code']}: {err['message']}"
            reason = reason.strip("\n")
        except json.decoder.JSONDecodeError:
            reason = self.reason
        return f"{self.description}. Reason: {reason}"


def with_api_logs(loglevel):
    """
    Handles logging for api endpoints
    """

    def decorator(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except APIError as error:
                logger.log(
                    loglevel,
                    "%s - %s",
                    kwargs.get("config")["cloudvps_project"],
                    error.to_str(),
                )
                raise

        return inner

    return decorator


def make_image_retention_from_template(
    project: Dict[str, Any]
) -> Dict[str, Any]:
    image_retention = copy.deepcopy(IMAGE_RETENTION_POLICY_TEMPLATE)
    image_retention["scope"]["ref"] = project["project_id"]
    return image_retention


def retentions_are_the_same(
    retention1: Dict[str, Any], retention2: Dict[str, Any]
) -> bool:
    left_side = copy.deepcopy(retention1)
    right_side = copy.deepcopy(retention2)
    # remove the ids, that should be the only difference
    left_side.pop("id", None)
    right_side.pop("id", None)
    return left_side == right_side


def make_harbor_project_names_mapping(
    harbor_projects: List[Dict[str, Any]]
) -> Dict[str, bool]:
    harbor_project_names: Dict[str, bool] = {}
    for harbor_project in harbor_projects:
        if harbor_project["name"].startswith(TOOLFORGE_PROJECT_PREFIX):
            harbor_project_names[
                harbor_project["name"][len(TOOLFORGE_PROJECT_PREFIX) :]  # noqa
            ] = True
    return harbor_project_names


def make_tool_names_mapping(tools: List[str]) -> Dict[str, bool]:
    tool_names: Dict[str, bool] = {}
    for tool in tools:
        tool_names[tool] = True
    return tool_names


def filter_harbor_projects_to_create(
    harbor_project_names: Dict[str, bool], tool_names: Dict[str, bool]
) -> List[str]:
    harbor_projects_to_create: List[str] = []
    for name in tool_names:
        if not harbor_project_names.get(name, False):
            harbor_projects_to_create.append(
                f"{TOOLFORGE_PROJECT_PREFIX}{name}"
            )
    return harbor_projects_to_create


def filter_harbor_projects_to_delete(
    harbor_project_names: Dict[str, bool], tool_names: Dict[str, bool]
) -> List[str]:
    harbor_projects_to_delete: List[str] = []
    for name in harbor_project_names:
        if not tool_names.get(name, False):
            harbor_projects_to_delete.append(
                f"{TOOLFORGE_PROJECT_PREFIX}{name}"
            )
    return harbor_projects_to_delete


def get_headers():
    """
    update default headers with recommended user-agent
    """
    # https://meta.wikimedia.org/wiki/User-Agent_policy
    headers = requests.utils.default_headers()
    headers.update({"User-Agent": USER_AGENT})
    return headers


def get_ldap_conn(config: Config):
    """
    Return a ldap connection

    Return value can be used as a context manager
    """
    servers = ldap3.ServerPool(
        [
            ldap3.Server(host, connect_timeout=1)
            for host in config["ldap"]["servers"]
        ],
        ldap3.ROUND_ROBIN,
        active=True,
        exhaust=True,
    )

    return ldap3.Connection(
        servers,
        read_only=True,
        user=config["ldap"]["user"],
        auto_bind=True,
        password=config["ldap"]["password"],
    )


@with_api_logs(loglevel=logging.ERROR)
def find_tools(config: Config) -> List[str]:
    """
    Return list of tools, from canonical LDAP source
    """
    ldap_dn = f"ou=people,ou=servicegroups,{config['ldap']['basedn']}"
    try:
        with get_ldap_conn(config=config) as conn:
            response = conn.extend.standard.paged_search(
                search_base=ldap_dn,
                search_filter=f"(cn={config['cloudvps_project']}.*)",
                attributes=["cn"],
                paged_size=1000,
                time_limit=5,
                generator=True,
            )

            users = []
            for tool in response:
                name = tool["attributes"]["cn"][0].split(
                    config["cloudvps_project"] + ".", 1
                )[1]
                if name:
                    users.append(name)
        return users

    except Exception as error:
        raise APIError(
            description="Request to get list of tools from LDAP failed",
            reason=str(error),
        ) from error


@with_api_logs(loglevel=logging.ERROR)
def make_harbor_request(
    config: Config,
    method: Literal["get", "post", "delete", "put"],
    path: str,
    error_desc: str,
    data: Optional[Dict[str, Any]] = None,
) -> requests.Response:
    kwargs = {
        "url": f"{config['base_harbor_api']}/{path}",
        "auth": (config["auth_username"], config["auth_password"]),
        "headers": get_headers(),
        "timeout": 60,
    }

    if method in ["post", "put"]:
        kwargs["json"] = data

    method_fn = getattr(requests, method)

    try:
        response = method_fn(**kwargs)
        response.raise_for_status()
        return response.json()

    except requests.exceptions.RequestException as error:
        reason = getattr(getattr(error, "response", object()), "text", None)
        reason = reason if reason else str(error)
        raise APIError(description=error_desc, reason=reason) from error


def get_harbor_projects(config: Config) -> List[Dict[str, Any]]:
    """Return list of harbor projects"""

    harbor_projects = make_harbor_request(
        config=config,
        method="get",
        path="projects?page_size=-1",
        error_desc="Request to get list of harbor projects failed",
    )

    logger.debug(
        "%s - Got %s projects!",
        config["cloudvps_project"],
        len(harbor_projects),
    )
    return harbor_projects


def delete_harbor_project(config: Config, harbor_project: str):
    make_harbor_request(
        config=config,
        method="delete",
        path=f"projects/{harbor_project}",
        error_desc=(
            f"Request to delete harbor project {harbor_project} failed"
        ),
    )

    logger.info(
        "%s - Successfully deleted harbor project %s",
        config["cloudvps_project"],
        harbor_project,
    )


def create_harbor_project(config: Config, harbor_project: str):
    make_harbor_request(
        config=config,
        method="post",
        path="projects",
        data={
            "project_name": harbor_project,
            "public": True,
            "registry_id": None,
        },
        error_desc=(
            f"Request to create harbor project {harbor_project} failed"
        ),
    )

    logger.info(
        "%s - Successfully created harbor project %s",
        config["cloudvps_project"],
        harbor_project,
    )


def get_image_retention_policy(
    config: Config, retention_id: int
) -> Dict[str, Any]:
    return make_harbor_request(
        config=config,
        method="get",
        path=f"retentions/{retention_id}",
        error_desc=(
            f"Request to get retention policy with ID {retention_id} failed"
        ),
    )


def update_image_retention_policy(config: Config, retention: Dict[str, Any]):
    make_harbor_request(
        config=config,
        method="put",
        path=f"retentions/{retention['id']}",
        data=retention,
        error_desc=(
            f"Request to update retention policy with ID {retention['id']} "
            "failed"
        ),
    )

    logger.debug(
        (
            "%s - Updated retention policy for project with ID %s as "
            "the retention with ID %s did not match"
        ),
        config["cloudvps_project"],
        retention["scope"]["ref"],
        retention["id"],
    )


def create_image_retention_policy(config: Config, retention: Dict[str, Any]):
    make_harbor_request(
        config=config,
        method="post",
        path="retentions",
        data=retention,
        error_desc=(
            f"Request to create image retention policy for project with ID "
            f"{retention['scope']['ref']} failed"
        ),
    )


def delete_harbor_projects(
    config: Config, harbor_projects_to_delete: List[str]
):
    for harbor_project in harbor_projects_to_delete:
        try:
            delete_harbor_project(config=config, harbor_project=harbor_project)
        except APIError:
            pass  # Do not interrupt execution when api errors are encountered


def create_harbor_projects(
    config: Config, harbor_projects_to_create: List[str]
):
    for harbor_project in harbor_projects_to_create:
        try:
            create_harbor_project(config=config, harbor_project=harbor_project)
        except APIError:
            pass  # Do not interrupt execution when api errors are encountered


def ensure_image_retention_policy_is_set_for_project(
    harbor_project: Dict[str, Any], config: Config
):
    current_retention_id = int(
        harbor_project.get("metadata", {}).get("retention_id", 0)
    )
    new_retention = make_image_retention_from_template(project=harbor_project)

    try:
        if current_retention_id:
            existing_retention = get_image_retention_policy(
                retention_id=current_retention_id, config=config
            )
            if retentions_are_the_same(existing_retention, new_retention):
                logger.debug(
                    (
                        "%s - Skipping project %s as it "
                        "already has a retention (id=%s)"
                    ),
                    config["cloudvps_project"],
                    harbor_project["name"],
                    current_retention_id,
                )
                return

            new_retention["id"] = current_retention_id
            update_image_retention_policy(
                config=config, retention=new_retention
            )

        else:
            create_image_retention_policy(
                config=config, retention=new_retention
            )

    except APIError:
        # Do not interrupt execution when api errors are encountered
        pass


def manage_image_retention_policy_per_project(
    config: Config, harbor_projects: List[Dict[str, Any]]
):
    if config.get("do_retentions", None):
        logger.info(
            "%s - Ensuring retentions are set for all projects",
            config["cloudvps_project"],
        )

        for harbor_project in harbor_projects:
            if harbor_project["name"].startswith(TOOLFORGE_PROJECT_PREFIX):
                ensure_image_retention_policy_is_set_for_project(
                    config=config, harbor_project=harbor_project
                )
    else:
        logger.info(
            "%s - Skipping retentions check", config["cloudvps_project"]
        )


def maintain_harbor(config: Config):
    tools = find_tools(config=config)

    harbor_projects = get_harbor_projects(config=config)

    harbor_project_names = make_harbor_project_names_mapping(
        harbor_projects=harbor_projects
    )

    tool_names = make_tool_names_mapping(tools=tools)

    harbor_projects_to_create = filter_harbor_projects_to_create(
        harbor_project_names=harbor_project_names, tool_names=tool_names
    )
    # harbor_projects_to_delete = filter_harbor_projects_to_delete(
    #     harbor_project_names=harbor_project_names,
    #     tool_names=tool_names,
    # )
    # delete_harbor_projects(
    #     config=config,
    #     harbor_projects_to_delete=harbor_projects_to_delete,
    # )
    create_harbor_projects(
        config=config, harbor_projects_to_create=harbor_projects_to_create
    )
    manage_image_retention_policy_per_project(
        config=config, harbor_projects=harbor_projects
    )


def main():
    argparser = argparse.ArgumentParser()

    argparser.add_argument(
        "--config", help="Path to YAML config file", type=Path, required=True
    )

    argparser.add_argument(
        "--debug", help="Turn on debug logging", action="store_true"
    )

    args = argparser.parse_args()

    out_handler = logging.StreamHandler(sys.stdout)
    out_handler.setLevel(logging.DEBUG)
    out_handler.addFilter(
        lambda record: record.levelno in (logging.DEBUG, logging.INFO)
    )

    err_handler = logging.StreamHandler(sys.stderr)
    err_handler.setLevel(logging.ERROR)

    log_lvl = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(
        level=log_lvl,
        format="%(asctime)s - %(levelname)s - %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
        handlers=[out_handler, err_handler],
    )
    logging.captureWarnings(True)

    with args.config.open("r", encoding="utf8") as f:
        config = yaml.safe_load(f)

    logging.debug("Loaded config from %s", str(args.config))

    with Path(config["ldap_config"]).open("r", encoding="utf8") as f:
        ldap_config = yaml.safe_load(f)

    for project, project_config in config["cloudvps_projects"].items():
        logger.info("########## Starting run on project %s", project)
        maintain_harbor(
            config={
                "cloudvps_project": project,
                "ldap": ldap_config,
                **project_config,
            }
        )


if __name__ == "__main__":
    main()
