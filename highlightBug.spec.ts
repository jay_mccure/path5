const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

const java = 12;
const optional = `code${window?.console ?? true}`;
const classes = `header ${ isLargeScreen() ? '' :
  `icon-${item.isCollapsed ? 'expander' : 'collapser'}` }`;
  

const SELECTED_PROJECT_SETTINGS_KEY = 'selectedProjectSettings';

export const convertProjectToSetting = ({
  account,
  project,
  pointer,
}: ProjectInRepository): SelectedProjectSetting => ({
  accountId: account.id,
  namespaceWithPath: project.namespaceWithPath,
  remoteName: pointer.remote.name,
  remoteUrl: pointer.urlEntry.url,
  repositoryRootPath: pointer.repository.rootFsPath,
});

export interface SelectedProjectStore {
  addSelectedProject(selectedProject: SelectedProjectSetting): Promise<void>;
  clearSelectedProjects(rootFsPath: string): Promise<void>;
  init(context: ExtensionContext): void;
  readonly onSelectedProjectsChange: vscode.Event<readonly SelectedProjectSetting[]>;
  readonly selectedProjectSettings: SelectedProjectSetting[];
}

